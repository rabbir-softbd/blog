<p>Dummy Blog with Laravel Voyager</p>

<h6>For starting this project you need to follow below steps:</h6>
<ol>
	<li>Rename .env.example file to .env inside your project root and fill the database information. (windows wont let you do it, so you have to open your console cd your project root directory and run mv .env.example .env )</li>
	<li>Open the console and cd your project root directory</li>
	<li>Run composer install or php composer.phar install</li>
	<li>Run php artisan key:generate</li>
	<li>Run php artisan migrate</li>
	<li>Run php artisan voyager:install --with-dummy</li>
	<li>Run php artisan serve</li>
</ol>

##### You can now access your project at localhost:8000 :) <br>
##### Admin URL localhost:8000/admin

##### Admin Access: <br>
email: admin@admin.com<br>
password: password